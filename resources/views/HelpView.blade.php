@extends('LayoutView')
@section('content')
{{-- $user = Auth::user();
        
        dd($user->only(['role', 'id'])); --}}
<section class="container white-block">
    <div class="row justify-content-start">

        <h1>Панель підтримки абонентів</h1>
        <h2 style="">Доброго дня <span class="color-green">{{ $user['name'] }}</span></h2>
    
   
  
    {{-- {{ dd($tariff) }} --}}
  {{-- {{ dd($abonent) }} --}}

    
    
            @if ($errors->any())
            <br>
        
              <div class="color-red errors col-10">
                  
                      @foreach ($errors->all() as $error)
                          <p>{{ $error }}</p>
                      @endforeach
              
              </div>
                @endif
       
      
        {{-- {{ dd($tariff) }} --}}
      {{-- {{ dd($abonent) }} --}}
        </div>
    </section>
      <section class="container white-block">
           







        <h2 >Список абонентів</h2>
          <div class="row justify-content-start ">
              <table class="col-11">
                  <tbody>
                  <tr>
                    <th>ПІБ або назва компанії</th>
                    <th>Пошта</th>
                    <th>Телефон</th>
                    
                    <th>Особистий рахунок</th>
                    <th>Баланс</th>
                    
                    <th>Редагувати</th>
                    <th>Видалити</th>
                  </tr>
            
                  {{-- <dd>{{ $operations }}</dd> --}}
                  @foreach($users as $usr)
                  <tr>
                  <td>{{ $usr->name }}</td>
                  <td>{{ $usr->email }}</td>
                  <td>{{ $usr->abonent->phone }}</td>
                  <td>{{ $usr->abonent->id }}</td>
                  <td>{{ $usr->abonent->balance }}</td>
                  @foreach($roles as $rol)
                  @if($usr->role_id == $rol->id)
                  
                  <td><a href="{{ route('editUserView', [$usr->id, $rol->role]) }}"title='Натисніть, щоб редагувати користувача'>Редагувати</a></td>
                  <td><a href="{{ route('deleteAbonent', [$usr->id]) }}title='Натисніть, щоб видалити користувача'">Видалити</a></td>
                  @endif
                  @endforeach
                  </tr>
                  @endforeach
                  
                  
                </tbody>
              </table>
          </div>
          <div class="pagination">
            <div>
          {{ $users->links() }}  
            </div></div>
          

 
      
            <div class="button-panel">
              <div class="col">
                <h2 >Пошук абонента</h2>
                <form style="margin-bottom: 20px" id="serarchForm" action="{{ @route('searchAbonent') }}" method="post">
                  @csrf
                  <p class="form-text-info">Особистий рахунок</p>
                  <input type="text" name="abonent_id">
                  
                  <span class="button" id="search-form-button">ПОШУК</span>
                  
                  <input id="search-form-submit" type="submit" class="" style="display: none;">
                </form>
                  
            
                
                
                  <a href="{{ route('report') }}" class="link-cabinet color-gray2" style="margin-left: 0">Друк місячного звіту</a>
                  <a href="{{ route('sortData') }}" class="link-cabinet color-gray2" style="margin-left: 0">Зворотні дзвінки</a>
            </div> 
      <h2 style="margin-top: 50px;">Створити абонента</h3>
        <dd>
          
           </dd> 
        <div class="col">
          <form id="registerForm" action="{{ @route('CreateAbonent') }}" method="post">
            @csrf
            <p class="form-text-info">ПІБ або назва компанії</p>
            <input type="text" name="user_name">
            <p class="form-text-info">Адресса</p>
            <input type="text" name="tariff_adress">
            <p class="form-text-info">Телефон</p>
            <input type="tel" name="abonent_phone">
            <p class="form-text-info">Електрнна пошта</p>
            <input type="email" name="user_email">
            <p class="form-text-info">Пароль</p>
            <input type="password" name="user_password">
            
            <p class="form-text-info" >Тариф</p>
            <select form="registerForm" name="tariff_id">
              @foreach($tariffs as $tariff)
              <option value="{{ $tariff->id }}">{{ $tariff->name }}</option>
              @endforeach
            </select><br>
            
            <div class="checkbox-border" id="checkbox-border">
              <img id="checkbox" src="{{ asset('img/check.svg') }}" alt="">
            </div>
            <span class="color-gray2 form-text-afert">Абонент надає угоду на обробку персональних даних,<br> та приймає умови <a class="color-gray2" href="{{ asset("file/afert.pdf") }}">договору</a></span>
            
            <div>

              <span class="button" id="main-form-button">ЗАРЕЄСТРУВАТИ</span>
            </div>
            
            
      
      
            <input id="main-form-submit" type="submit" class="" style="display: none;">
          </div>
      
          </form>
        </div>
    
    






            
      </section>
      
    
    
    
    
    @endsection
    
    @section('js')
    <script src="{{asset("js/help.js")}}"></script>
    @endsection
    
