@extends('LayoutView')
@section('content')
<section class="container white-block">
    <div class="row justify-content-start">

   
  <h2 style="margin-top: 50px;">Змінити тариф</h3>

  <form id="editFormTariff" action="{{ route("editTariffSubmit") }}" method="post">
    @csrf
    <div class="">
      <input type="text" name="id" value="{{ $tariff->id }}" style="display: none">

      <p class="form-text-info" >Назва тарифу</p>
      <input type="text" name="name" value="{{ $tariff->name }}">
      
      <p class="form-text-info">Вартість</p>
      <input type="text" name="price" value="{{ $tariff->price }}">
      
      <p class="form-text-info">Опис тарифу</p>
      <textarea class="" name="content">{{ $tariff->content }}</textarea>
    
    </div>
    
    <span class="button" id="form-button-tariff">ЗМІНИТИ</span>
    <input id="form-submit-tariff" type="submit" class="" style="display: none;">
</form>



    </section>
   
    
@endsection

@section('js')
<script src="{{asset("js/admin.js")}}"></script>
@endsection
