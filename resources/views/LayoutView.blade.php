<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MRPL.TRASH</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">    
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset("css/style.css") }}">
    
</head>
<body class="background-color-gray">
  <header>

    <nav class="navbar navbar-expand-lg navbar-light bg-light print" media="print">
        <div class="container">
          <a class="navbar-brand d-inline-flex" href="{{ url('/') }}"><p class="color-green no-margin">MRPL</p><p class="color-gray no-margin">.TRASH</p></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link color-gray" aria-current="page" href="{{ url('/') }}">ГОЛОВНА</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-gray" href="{{ url('/contacts') }}">О НАС</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link color-gray" href="{{ url('/tariffs') }}">ТАРИФИ</a>
                </li>
                
                @if($role=='abonent')

                <li class="nav-item">
                  <a class="nav-link color-gray"href="{{ url('/abonent') }}" >ОСОБИСТИЙ КАБІНЕТ</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link color-gray"href="{{ url('/logout') }}" >ВИХІД</a>
                </li>
                @elseif($role=='admin')
                <li class="nav-item">
                  <a class="nav-link color-gray"href="{{ url('/admin') }}" >ПАНЕЛЬ АДМІНІСТРАТОРА</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link color-gray"href="{{ url('/logout') }}" >ВИХІД</a>
                </li>
                @elseif($role=='helper')
                <li class="nav-item">
                  <a class="nav-link color-gray"href="{{ url('/help') }}" >ПАНЕЛЬ ПІДТРИМКИ</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link color-gray"href="{{ url('/logout') }}" >ВИХІД</a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link color-gray"href="{{ url('/login') }}" >ВХІД</a>
                </li>

                @endif
                
            </ul>
          </div>
        </div>
      </nav>
  </header>
<section class="main-content">
     @yield('content')
</section>
<footer class="background-color-focus-gray print" media="print">
  <div class="container">
  <div class="row">  

    <div class="col">
      <h4 class="color-main footer-title">Контакти</h4>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="mailto:qwerty@mail.com">qwerty@mail.com</a>
      <a class="footer-link d-block" href="https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%9A%D0%B0%D1%80%D0%BB%D0%B0+%D0%9C%D0%B0%D1%80%D0%BA%D1%81%D0%B0+38,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA,+%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C/data=!4m2!3m1!1s0x46dbcfc13b959575:0x40e1f173f49f6923?sa=X&ved=2ahUKEwj21r3Al8nwAhVWhf0HHUJCATYQ8gEwAHoECAUQAQ">Вул. Карла Маркса 38</a>
    </div> 
    <div class="col">
      <h4 class="color-main footer-title">Контакти</h4>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="tel:+380663399613">+380663399612</a>
      <a class="footer-link d-block" href="mailto:qwerty@mail.com">qwerty@mail.com</a>
      <a class="footer-link d-block" href="https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%9A%D0%B0%D1%80%D0%BB%D0%B0+%D0%9C%D0%B0%D1%80%D0%BA%D1%81%D0%B0+38,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA,+%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C/data=!4m2!3m1!1s0x46dbcfc13b959575:0x40e1f173f49f6923?sa=X&ved=2ahUKEwj21r3Al8nwAhVWhf0HHUJCATYQ8gEwAHoECAUQAQ">Вул. Карла Маркса 38</a>
    </div> 
    <div class="col">
      <h4 class="color-main footer-title">Контакти</h4>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="mailto:qwerty@mail.com">qwerty@mail.com</a>
      <a class="footer-link d-block" href="https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%9A%D0%B0%D1%80%D0%BB%D0%B0+%D0%9C%D0%B0%D1%80%D0%BA%D1%81%D0%B0+38,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA,+%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C/data=!4m2!3m1!1s0x46dbcfc13b959575:0x40e1f173f49f6923?sa=X&ved=2ahUKEwj21r3Al8nwAhVWhf0HHUJCATYQ8gEwAHoECAUQAQ">Вул. Карла Маркса 38</a>
    </div> 
    <div class="col">
      <h4 class="color-main footer-title">Контакти</h4>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="tel:+380663399612">+380663399612</a>
      <a class="footer-link d-block" href="mailto:qwerty@mail.com">qwerty@mail.com</a>
      <a class="footer-link d-block" href="https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%9A%D0%B0%D1%80%D0%BB%D0%B0+%D0%9C%D0%B0%D1%80%D0%BA%D1%81%D0%B0+38,+%D0%9C%D0%B8%D0%BD%D1%81%D0%BA,+%D0%91%D0%B5%D0%BB%D0%B0%D1%80%D1%83%D1%81%D1%8C/data=!4m2!3m1!1s0x46dbcfc13b959575:0x40e1f173f49f6923?sa=X&ved=2ahUKEwj21r3Al8nwAhVWhf0HHUJCATYQ8gEwAHoECAUQAQ">Вул. Карла Маркса 38</a>
    </div> 
    
  </div>
</div>
<a class="copy" href="#">© MRPL.TRASH 2021-2021</a>
</footer>






<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
@yield('js')
</body>

{{-- <script src="{{asset("js/scripts.js")}}"></script> --}}
</html>