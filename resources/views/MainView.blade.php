@extends('LayoutView')
@section('content')


 <section>
    <div id="MainCarousel" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-indicators">
          <div type="button" data-bs-target="#MainCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></div>
          <div type="button" data-bs-target="#MainCarousel" data-bs-slide-to="1" aria-label="Slide 2"></div>
          <div type="button" data-bs-target="#MainCarousel" data-bs-slide-to="2" aria-label="Slide 3"></div>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{ asset("img/MainSlide1.png") }}" class="d-block w-100" alt="">
          <div class="carousel-caption d-none d-md-block">
            <h5>Контейнер в кожне місце</h5>
            <p>Установіть контейнер в яке завгодно місце<br>
              Ціна залишиться незмінною</p>
            <a href="#" class="button">ДЕТАЛЬНІШЕ</a>
          </div>
        </div>
        <div class="carousel-item">
          <img src="{{ asset("img/MainSlide1.png") }}" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>Контейнер в кожне місце</h5>
            <p>Установіть контейнер в яке завгодно місце<br>
              Ціна залишиться незмінною</p>
            <a href="#" class="button">ДЕТАЛЬНІШЕ</a>
          </div>
        </div>
        <div class="carousel-item">
          <img src="{{ asset("img/MainSlide1.png") }}" class="d-block w-100" alt="...">
          <div class="carousel-caption d-none d-md-block">
            <h5>Контейнер в кожне місце</h5>
            <p>Установіть контейнер в яке завгодно місце<br>
              Ціна залишиться незмінною</p>
            <a href="#" class="button">ДЕТАЛЬНІШЕ</a>
          </div>
        </div>
      </div>
      
    </div>
  </section>

    <section>
      <div class="container">
       
          <div class="row sentences">
            <div class="col text-center">
              <img src="{{ asset('img/Sentence1.png') }}" alt="">
              <h3>Ми сортуємо</h3>
              <p>В нащій компанії дуже чильно відносяться до сортировки сміття, також ми пропонуємо за низьким тарифом постивити вам контейнери для побутового сміття різного типу. </p>
              <a href="{{ url('/tariffs') }}" class="button d-inline-block">ДЕТАЛЬНІШЕ</a>
          </div>
          <div class="col text-center">
              <img src="{{ asset('img/Sentence1.png') }}" alt="">
              <h3>Ми переробляємо</h3>
              <p>В нащій компанії дуже чильно відносяться до сортировки сміття, також ми пропонуємо за низьким тарифом постивити вам контейнери для побутового сміття різного типу. </p>
              <a href="{{ url('/tariffs') }}" class="button d-inline-block">ДЕТАЛЬНІШЕ</a>
            </div>
            <div class="col text-center">
              <img src="{{ asset('img/Sentence1.png') }}" alt="">
              <h3>Ми діємо</h3>
              <p>В нащій компанії дуже чильно відносяться до сортировки сміття, також ми пропонуємо за низьким тарифом постивити вам контейнери для побутового сміття різного типу. </p>
              <a href="{{ url('/tariffs') }}" class="button d-inline-block">ДЕТАЛЬНІШЕ</a>
            </div>
          </div>
        </div>

    </section>

<section class="container">
<div class="row background-color-main main-form">
  <div class="col">
    <h2 class="color-focus-gray">Як стати абонентом?</h2>
    <ul class="color-gray2">
      <li>Заповни форму</li>
      <li>Поставь галочку</li>
      <li>Натисни “РЕЕСТРАЦІЯ”</li>
    </ul>
    <h2 class="color-focus-gray">Або</h2>
    <a class="form-phone" href="tel:+380663399612" class="color-gray2">Зателефонуй нам</a>

    @if ($errors->any())
    <br>
      <div class="color-red errors">
          
              @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
              @endforeach
      
      </div>
        @endif

  </div>

  <div class="col">
    <form id="registerForm" action="{{ @route('CreateAbonent') }}" method="post">
      @csrf
      <p class="form-text-info">ПІБ або назва компанії</p>
      <input type="text" name="user_name">
      <p class="form-text-info">Адресса</p>
      <input type="text" name="tariff_adress">
      <p class="form-text-info">Телефон</p>
      <input type="tel" name="abonent_phone">
      <p class="form-text-info">Електрнна пошта</p>
      <input type="email" name="user_email">
      <p class="form-text-info">Пароль</p>
      <input type="password" name="user_password">
      
      <p class="form-text-info" >Тариф</p>
      <select form="registerForm" name="tariff_id">
        @foreach($tariffs as $tariff)
        <option value="{{ $tariff->id }}">{{ $tariff->name }}</option>
        @endforeach
      </select><br>
      
      <div class="checkbox-border" id="checkbox-border">
        <img id="checkbox" src="{{ asset('img/check.svg') }}" alt="">
      </div>
      <span class="color-gray2 form-text-afert">Надаю угоду на обробку персональних даних,<br> та приймаю умови <a class="color-gray2" href="{{ asset("file/afert.pdf") }}">договору</a></span>
      
      
      <span class="button" id="main-form-button">РЕЄСТРАЦІЯ</span>
      
      


      <input id="main-form-submit" type="submit" class="" style="display: none;">
    </div>

    </form>
  </div>
  
</div>
</section>
@endsection
@section('js')
<script src="{{asset("js/main.js")}}"></script>
@endsection