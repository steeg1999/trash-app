@extends('LayoutView')
@section('content')
{{-- $user = Auth::user();
        
        dd($user->only(['role', 'id'])); --}}
<section class="container white-block">
    <div class="row justify-content-start">

        <h1>Панель підтримки абонентів</h1>
        <h2 style="">Доброго дня <span class="color-green">{{ $user['name'] }}</span></h2>
    
   
  
    {{-- {{ dd($tariff) }} --}}
  {{-- {{ dd($abonent) }} --}}

    
    
            @if ($errors->any())
            <br>
        
              <div class="color-red errors col-10">
                  
                      @foreach ($errors->all() as $error)
                          <p>{{ $error }}</p>
                      @endforeach
              
              </div>
                @endif
       
      
        {{-- {{ dd($tariff) }} --}}
      {{-- {{ dd($abonent) }} --}}
        </div>
    </section>
      <section class="container white-block">
        <h2 >Заявки абонентів</h2>
        <div class="row justify-content-start ">
            <table class="col-11">
                <tbody>
                <tr>
                  <th>ПІБ або назва компанії</th>
                  <th>Телефон</th>
                  <th>Пошта</th>
                  <th>Особистий рахунок</th>
                  <th>Баланс</th>
                  <th><a href="{{ $sortCheck }}" itle="Натисніть для сортування за датою">Дата заявки</a></th>
                  <th>Редагувати</th>
                  <th style="width: 120px"><a href="{{ $sortData }}" title="Натисніть для сортування за статусом">Статус</a></th>
                </tr>
          
                {{-- <dd>{{ $operations }}</dd> --}}
                @foreach($callbacks as $callback)
                <tr>
                <td>{{ $callback->abonent->user->name }}</td>
                <td>{{ $callback->abonent->phone }}</td>
                <td>{{ $callback->abonent->user->email }}</td>
                <td>{{ $callback->abonent->id }}</td>
                <td>{{ $callback->abonent->balance }}</td>
                <td>{{ $callback->created_at }}</td>
                <td><a href="{{ route('editUserView', [$callback->abonent->user->id, 'abonent']) }}" title='Натисніть, щоб редагувати користувача'>Редагувати</a></td>
                @if($callback->status==0)
                <td><a href="{{ route('confirmCallback', [$callback->id]) }}" style="width: 120px" title='Натисніть, щоб виконати заявку'>Очікує</a></td>
                @else
                <td style="width: 120px">Виконана✓</a></td>
                @endif
              
                @endforeach
                
                
              </tbody>
            </table>
        </div>

        <div class="pagination">
          <div>
            {{ $callbacks->links() }}   
        </div>
        </div>
        








            
      </section>
      
    
    
    
    
    @endsection
    
    @section('js')
    <script src="{{asset("js/help.js")}}"></script>
    @endsection
    
