@extends('LayoutView')
@section('content')
{{-- $user = Auth::user();
        
        dd($user->only(['role', 'id'])); --}}
<section class="container white-block">
  <div class="row justify-content-center" >
  <h2 style="text-align: center">Щомісячний звіт</h2>
  <br>
  <br>
  {{-- 'sumDebt'=>$sumDebt,
  'sumAbonentDebt'=>$sumAbonentDebt,
  'sumAbonent'=>$sumAbonent,
  'operations'=>$operations,
  'transactions'=>$transactions,
  'date'=>$date, --}}
  <table class="col-10">
    <tr>
    <th>Назва</th>
    <th>Сума</th>
    </tr>
    <tr>
    <td>Борг абонентів</td>
    <td>{{ $result['sumDebt'] }}</td>
    </tr>
    <tr>
    <td>Кількість абонентів боржників</td>
    <td>{{ $result['sumAbonentDebt'] }}</td>
    </tr>
    <tr>
    <td>Кількість абонентів</td>
    <td>{{ $result['sumAbonent'] }}</td>
    </tr>
    <tr>
    <td>Надані послуги</td>
    <td>{{ $result['operations'] }}</td>
    </tr>
    <tr>
    <td>Поповнення рахунків</td>
    <td>{{ $result['transactions'] }}</td>
    </tr>
  </table>
     
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="col-10">

      <p class="" >{{ $result['date'] }} <span style="float: right;">{{ $user->name }}</span></p>       
      <div>
        <div class="button print" media="print" id="button">ДРУК</div>
        </div>
    </div>
  </div>
    
    
      </section>



@endsection

@section('js')
<script src="{{asset("js/receipt.js")}}"></script>
@endsection
