@extends('LayoutView')
@section('content')
{{-- $user = Auth::user();
        
        dd($user->only(['role', 'id'])); --}}
<section class="container white-block">
  
      <h2 style="">Особистий рахунок: <span class="color-green">{{ $abonent['id'] }}</span></h2>
      <h2 style="">Баланс: <span class="color-green">{{ $abonent['balance'] }}</span>грн</h2>
      <h2 style="">Щомісячне списання: <span class="color-green">{{ $pay }}</span>грн</h2>
      <h2 style="">Рекомендована сума поповнення: <span class="color-green">{{ $recomendPay }}</span>грн</h2>
    
   <br>
    <h2 style="">Реквезити поповнення:</h2>
    <p class="" >IBAN: UA213223130000026007233566001</p>       
    <p class="" >Назва організації: ООО 'MRPL.TRASH'</p>       
    <p class="" >ЄДРПОУ: 12345678</p>       
    <p class="" >Особистий рахунок: {{ $abonent['id'] }}</p>       
    
    <br>
    <h2 style="">Ващі дaні:</h2>
    <p class="" >ПІБ або назва компанії: {{ $user['name'] }}</p>       
    <p class="">Телефон: {{ $abonent['phone'] }}</p>
    <p class="">Електрнна пошта: {{ $user['email'] }}</p>
    <p class="" >Особистий рахунок: {{ $abonent['id'] }}</p>       
    
    
    
        <h4 class="history">Ващі адреси</h4>
        
        <table class="col-12">
          <tbody>
            <tr>
              <th>Адреса</th>
              <th>Назва тарифу</th>
              <th>Щомісячна вартість</th>
              
            </tr>
           


            @foreach($abonentTariffs as $abonTariff)
            <tr>
              <td>{{ $abonTariff->adress }}</td>
              @foreach($tariffs as $tariff)
              @if($abonTariff->tariff_id==$tariff->id)
              <td>{{ $tariff->name }}</td>
              <td>{{ $tariff->price }}</td>
              @endif
              
              @endforeach
              
            </tr>
            @endforeach
            
          </div>
          
        </tbody>
      </table>
      
      <div class="button print" media="print" id="button">ДРУК</div>
 

      </div></section>



@endsection

@section('js')
<script src="{{asset("js/receipt.js")}}"></script>
@endsection
