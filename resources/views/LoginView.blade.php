@extends('LayoutView')
@section('content')

<section class="white-block login-block">

<h2>Вхід</h2>
    <form action="LoginController/login" method="POST">
        @csrf
        <p class="form-text-info">Електрнна пошта</p>
      <input type="email" class="input-string" name="email">
      <p class="form-text-info">Пароль</p>
      <input type="password" name="password" class="input-string">
      @error('email')
          <p class="color-red">{{ $message }}</p>
      @enderror
      @error('password')
          <p class="color-red">{{ $message }}</p>
      @enderror
      
      <div class="button" id="form-button">ВХІД</div>
      <input id="form-submit" type="submit" class="" style="display: none;">
    </form>

</section>
@endsection
@section('js')
<script src="{{asset("js/login.js")}}"></script>
@endsection