@extends('LayoutView')
@section('content')

<section class="white-block login-block">

<h2>Поповнення балансу</h2>
    <form action="{{ route('payBalance') }}" method="POST">
        @csrf

     
      <p class="form-text-info" >Сума поповнення</p>
      <input style="width: 220px;" type="text" name="balance" class="input-string">
      
      @if ($errors->any())
        
        <br>
    
          <div class="color-red errors col-10">
              
                  @foreach ($errors->all() as $error)
                      <p>{{ $error }}</p>
                  @endforeach
          
          </div>
            @endif
      
      <div class="button" id="form-button">ПОПОВНИТИ</div>
      <input id="form-submit" type="submit" class="" style="display: none;">
    </form>

</section>
@endsection
@section('js')
<script src="{{asset("js/login.js")}}"></script>
@endsection