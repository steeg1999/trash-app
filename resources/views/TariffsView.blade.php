@extends('LayoutView')
@section('content')

<section class="container white-block">
<div class="row">

<h1>Ми пропонуємо такі тарифи</h1>

@foreach($tariffs as $tariff)
<div class="tariff">
<h2>Тариф: <span class="color-green">{{ $tariff->name }}</span></h2> 
<h3>Щомісячний платіж: <span class="color-green">{{ $tariff->price }}</span>грн</h3>
<p><span class="">{{ $tariff->content }}</span></p>
</div>
@endforeach

<a class="form-phone" href="tel:+380663399612" class="color-gray2">Зателефонуй нам</a>
</section>


@endsection