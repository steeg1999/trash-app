@extends('LayoutView')
@section('content')
{{-- $user = Auth::user();
        
        dd($user->only(['role', 'id'])); --}}
<section class="container white-block">
  <h1>Особистий кабінет абонента <span class="color-green">{{ $user['name'] }}</span></h1>
    <div class="row justify-content-start">
  
    <div class="col-6">
      <h2 style="">Особистий рахунок: №<span class="color-green">{{ $abonent['id'] }}</span></h2>
    </div>
    <div class="col-6">
      <h2 style="">Баланс: <span class="color-green">{{ $abonent['balance'] }}</span>грн</h2>
    </div>
    @if ($errors->any())
        
        <br>
    
          <div class="color-red errors col-10">
              
                  @foreach ($errors->all() as $error)
                      <p>{{ $error }}</p>
                  @endforeach
          
          </div>
            @endif
    <h4 class="history">Історія сплати та надання послуг</h4>
    

    <table class="col-10">
      <tbody>
      <tr>
        <th>Операція</th>
        <th>Кошти</th>
        <th>Дата</th>
      </tr>

      {{-- <dd>{{ $operations }}</dd> --}}
      @foreach($operations as $operation)
      <tr>
      <td>{{ $operation->name }}</td>
      <td>{{ $operation->money }}</td>
      <td>{{ $operation->created_at }}</td>
      </tr>
      @endforeach
      
    </div>
    
  </tbody>
</table>

<div class="pagination">
  <div>
  {{ $operations->links() }}    
</div>
</div>


{{-- {{ dd($tariff) }} --}}
  {{-- {{ dd($abonent) }} --}}

    </section>
   
    
    <section class="container white-block">
      <div class="row justify-content-start flex-row-reverse">
      <div class="col-10 ">
        <h2 style="">Дaні абонента:</h2>
        
        <form id="editForm" action="{{ @route('editAbonent') }}" method="post">
          @csrf
          <div class="row ">
          <div class="col-5">
            <p class="form-text-info" >ПІБ або назва компанії</p>
            <input type="text" name="user_name" value="{{ $user['name'] }}">
            
            <p class="form-text-info">Телефон</p>
            <input type="tel" name="abonent_phone" value="{{ $abonent['phone'] }}">
          </div>
          <input type="text" name="user_id" value="{{ $user['id'] }}" style="display: none">
          <div class="col-3">
            <p class="form-text-info">Електрнна пошта</p>
            <input type="email" name="user_email" value="{{ $user['email'] }}">
            
            <p class="form-text-info">Пароль</p>
            <input type="password" name="user_password">
            
            
          </div>
          
        <div class="button-panel">
            
          
          <span class="button" id="form-button">ЗМІНИТИ</span>
          <a href="{{ url()->current() }}"><img src="{{ asset('img/redo-alt-solid.svg') }}" alt="Оновити" class='redo' id="redo"></a>
          <a href="{{ route('deleteUser', [$user['id'],'abonent']) }}" class="link-cabinet color-gray2">Видалити користувача</a>
   
          <input id="form-submit" type="submit" class="" style="display: none;">
        </div>
    
        </form>

        
        

      </div>
      </div>
    </section>










    <section class="container white-block">
      <div class="row justify-content-start flex-row-reverse">
        <div class="col-10 ">
        <h2 style="">Тарифи</h2>
        
        <h4 class="history">Адреси користувача</h4>
        
        <table class="col-10">
          <tbody>
            <tr>
              <th>Адреса</th>
              <th>Назва тарифу</th>
              <th>Щомісячна вартість</th>
              <th>Видалити</th>
            </tr>
            
            {{-- <dd>{{ $operations }}</dd> --}}


            @foreach($abonentTariffs as $abonTariff)
            <tr>
              <td>{{ $abonTariff->adress }}</td>
              @foreach($tariffs as $tariff)
              @if($abonTariff->tariff_id==$tariff->id)
              <td>{{ $tariff->name }}</td>
              <td>{{ $tariff->price }}</td>
              @endif
              
              @endforeach


              
              <td><a href="{{ route( 'delAdress', $abonTariff->id)  }}">Видалити</a></td>
            </tr>
            @endforeach
            
          </div>
          
        </tbody>
      </table>
      
      
      
      
      
      
      
      
      <h4 class="history">Додати адресу</h4>
      <form id="addTariffForm" action="{{ route('addAdress') }}" method="post">
        @csrf
      <p class="form-text-info" >Адреса</p>
      <input style="display: none" type="text" name="user_id" value="{{ $user['id'] }}">
      <input type="text" name="adress">
      <p class="form-text-info" >Тариф</p>
    <select form="addTariffForm" name="tariff_id">
      @foreach($tariffs as $tariff)
      <option value="{{ $tariff->id }}">{{ $tariff->name }}</option>
      @endforeach
    </select>
    <input id="form-submit-addAdress" type="submit" class="" style="display: none;">
    <span class="button" id="form-button-addAdress">ДОДАТИ</span>
  </form>
 
  
  </div></div></section>











@endsection

@section('js')
<script src="{{asset("js/abonent.js")}}"></script>
@endsection
