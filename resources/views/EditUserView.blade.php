@extends('LayoutView')
@section('content')
<section class="container white-block">
    <div class="row justify-content-start">

   
  <h2 style="margin-top: 50px;">Змінити користувача</h3>

    <form id="editForm" action="{{ @route('editUser') }}" method="post">
      @csrf
      <div class="row ">
      <div class="col-5">
        <p class="form-text-info" >ПІБ </p>
        <input type="text" name="user_name" value="{{ $user['name'] }}">

      <input type="text" name="user_id" value="{{ $user['id'] }}" style="display: none">
      
        <p class="form-text-info">Електрнна пошта</p>
        <input type="email" name="user_email" value="{{ $user['email'] }}">
        
        <p class="form-text-info">Пароль</p>
        <input type="password" name="user_password">
        
        
      
      
    <div class="button-panel">
        
      
      <input id="form-submit" type="submit" class="" style="display: none;">
      <span class="button" id="form-button">ЗМІНИТИ</span>
      
      
      
    </div>
    
  </form>
  </div>
    
    
    <input id="form-submit-tariff" type="submit" class="" style="display: none;">
</form>



    </section>
   
    
@endsection

@section('js')
<script src="{{asset("js/user .js")}}"></script>
@endsection
