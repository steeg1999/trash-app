@extends('LayoutView')
@section('content')
{{-- $user = Auth::user();
        
        dd($user->only(['role', 'id'])); --}}
<section class="container white-block">
    <div class="row justify-content-start">

        <h1>Панель адміністратора</h1>
        <h2 style="">Доброго дня <span class="color-green">{{ $user['name'] }}</span></h2>
        @if ($errors->any())
        <br>
    
          <div class="color-red errors col-10">
              
                  @foreach ($errors->all() as $error)
                      <p>{{ $error }}</p>
                  @endforeach
          
          </div>
            @endif
   
  
    {{-- {{ dd($tariff) }} --}}
  {{-- {{ dd($abonent) }} --}}
    </div>
</section>
  <section class="container white-block">
    <div class="row justify-content-start">
  <h2>Тарифи</h4>
  <table class="">
    <tbody>
    <tr>
      <th>Назва</th>
      <th>Вартість</th>
      <th>Редагувати</th>
      <th>Видалити</th>
    </tr>
    @foreach($tariffs as $tariff)
    <tr>
        
      <td>{{ $tariff->name }}</td>
      <td>{{ $tariff->price }}</td>
      <td><a href="{{ route('editTariffView', $tariff->id) }}">Редагувати</a></td>
      <td><a href="{{ route('deletTariff', $tariff->id) }}">Видалити</a></td>
    </tr>     
    @endforeach
    
    </tbody>
  </table>
  
  <div>
  <h2 style="margin: 40px 0 0px;">Щомісячне списання</h2>
  <a href="{{ route('payAll') }}" id="form-submit-pay" style="display: none">
  </a>
    <span class="button" id="form-button-pay">СПИСАТИ</span>
  <span class="form-text-info">Натисніть для того щоб списати зу всіх абонентів кошти за тарифами</span>
  </div>
  

  <h2 style="margin-top: 50px;">Створити тариф</h3>

  <form id="createFormTariff" action="{{ route("createTariff") }}" method="post">
    @csrf
    <div class="">

      <p class="form-text-info" >Назва тарифу</p>
      <input type="text" name="name">
      
      <p class="form-text-info">Вартість</p>
      <input type="text" name="price" value="">
      
      <p class="form-text-info">Опис тарифу</p>
      <textarea class="" name="content"></textarea>
      
      
      
    </div>
    
    <span class="button" id="form-button-tariff">СТВОРИТИ</span>
    <input id="form-submit-tariff" type="submit" class="" style="display: none;">
</form>


    </section>
    <section class="container white-block">
      <div class="row justify-content-start flex-row-reverse">
          <h2 style="">Користувачі</h2>
            <table class="">
                <tbody>
                <tr>
                  <th>ПІБ або назва компанії</th>
                  <th>Пошта</th>
                  <th>Дата реєстрації</th>
                  <th>Роль</th>
                  <th>Редагувати</th>
                  <th>Видалити</th>
                </tr>
          
                {{-- <dd>{{ $operations }}</dd> --}}
                @foreach($users as $usr)
                <tr>
                <td>{{ $usr->name }}</td>
                <td>{{ $usr->email }}</td>
                <td>{{ $usr->created_at }}</td>
                @foreach($roles as $rol)
                @if($usr->role_id == $rol->id)
                <td>{{ $rol->role }}</td>
                <td><a href="{{ route('editUserView', [$usr->id, $rol->role]) }}">Редагувати</a></td>
                <td><a href="{{ route('deleteUser', [$usr->id, $rol->role]) }}">Видалити</a></td>
                @endif
                @endforeach
                </tr>
                @endforeach
                
              </div>
              
            </tbody>
          </table>
          <div class="pagination">
            <div>
          {{ $users->links() }}  
            </div></div>
        <div class="">
          <h2 style="">Створити користувача</h2>
          
          <form id="createForm" action="{{ route("CreateUser") }}" method="post">
            @csrf
            <div class="">
              
              <p class="form-text-info" >ПІБ</p>
              <input type="text" name="name">
              
              <p class="form-text-info">Електрнна пошта</p>
              <input type="email" name="email" value="">
              
              <p class="form-text-info">Пароль</p>
              <input type="password" name="password">
              <p class="form-text-info" >Тип користувача</p>
              <select form="createForm" name="role_id">
                @foreach($roles as $rol)
                @if($rol['role']!=='abonent')
                <option value="{{ $rol['id'] }}">{{ $rol['role'] }}</option>  
                @endif
                @endforeach
              </select>
              
            </div>
            
            <span class="button" id="form-button-user">СТВОРИТИ</span>
            <input id="form-submit-user" type="submit" class="" style="display: none;">
          </form>
          
          <div class="button-panel">
            
          </div>
          
         
  
          
          
  
        </div>
        </div>
      </section>
  




@endsection

@section('js')
<script src="{{asset("js/admin.js")}}"></script>
@endsection
