1. Вы можете переместить базу данных из папки проекта или запустить стартовую настройку проекта yourdoamin.com/make 
You can move the database from the project folder or run the project start setup yourdoamin.com/make

2. После переноса базы данных вам будет доступен проект с уже наполненными полями базы данных, достаточных для просмотра проекта. В случае стартовой настройки будут созданы  таблицы БД и 2 пользователя, с правами доступа "Администратор" и "Поддержка клиента"
After the database transfer, you will have access to the project with the database fields already filled in, sufficient to view the project. In the case of a start configuration, database tables and 2 users will be created, with access rights "Administrator" and "Customer support"

3. Стартовый логин пользователя типа "администратор": "admin@admin" пароль "password"
Стартовый  логин пользователя типа "Поддержка клиента": "help@help" пароль "password"
Starting login of user of type "administrator": "admin@admin" password "password"
Starting username of the "Customer Support" type: "help@help" password "password"