<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Abonent extends Model
{
    protected $table = 'abonents';
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function tariff()
    {
        return $this->belongsTo(tariffsModel::class);
    }
    public function abonentTariffs()
    {
        return $this->hasMany(AbonentsTariffs::class);
    }
}