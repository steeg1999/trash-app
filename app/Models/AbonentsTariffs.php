<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbonentsTariffs extends Model
{
    use HasFactory;
    public function tariff()
    {
        return $this->belongsTo(tariffsModel::class);
    }
    public function abonent()
    {
        return $this->belongsTo(Abonent::class);
    }
    // protected $table = 'abonents_tariffs';
}
