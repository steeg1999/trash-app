<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbonentsCallback extends Model
{
    use HasFactory;
    protected $table = 'abonents_callback';
    public function abonent()
        {
            return $this->belongsTo(abonent::class);
        }
}