<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class operationsModel extends Model
{
    use HasFactory;
    
    protected $table = 'operations';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

    public static function getAbonentOperations($abonent_id)
    {
        return DB::table('operations')->where('abonent_id' => $abonent_id)->paginate(10);
    }
}
