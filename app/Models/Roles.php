<?php

namespace App\Models;
use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;
    
    public static function getRoleById($id)
    {
        $role = Roles::where('id', $id)->first()->only('role');
        return $role = $role['role'];
    }
    public static function getIdByRole($role)
    {
        $id = Roles::where('role', $role)->first()->only('id');
        return $id = $id['id'];
    }
    public function user()
            {
                return $this->hasOne(User::class);
            }
    public static function currentRole()
    {
        $role = 'guest';
        if(Auth::check()){
            $role = Auth::user()->only('role_id');
            // dd($role['role_id']);
            $role = Roles::getRoleById($role['role_id']);
        }
        return $role;
    }
}
