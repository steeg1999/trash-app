<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Carbon\Carbon;


class usersModel extends Model
{
    use HasFactory, Notifiable;
    public $table = 'users';
    // public function setPasswordAttribute($password)
    // {
    //     $this->attributes['password'] = Hash::make($password);
    // }

    public static function uniqEmail($email)
    {
        if(DB::table('users')->where('email', $email)->exists())
        {
            return true;
        }
        else{
            return false;
        }
    }
    public static function insert($user)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        $user['created_at'] = $current_date_time;
        DB::table('users')->insert($user);
    }
    
    public function addUserAbonent($name, $email, $password)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        $id = DB::table('users')->insertGetId([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'role' => 'abonent',
            'created_at' => $current_date_time
        ]);
        return $id;
    }
    public static function updateUser($user)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        
        DB::table('users')
                ->where('id', $user['id'])
                ->update([
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'password' => Hash::make($user['password']),
                        'updated_at' => $current_date_time
                    ]);
    }
    public static function updateUserNoPassword($user)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        
        DB::table('users')
                ->where('id', $user['id'])
                ->update([
                        'name' => $user['name'],
                        'email' => $user['email'],
                        'updated_at' => $current_date_time
                    ]);
    }
    public static function deleteUser($id)
    {
        DB::table('user')->where('id', $id)->delete();
    }
    public static function editUser($user)
    {
        DB::table('user')->where('id', $user['id'])->update($user);
    }
}
