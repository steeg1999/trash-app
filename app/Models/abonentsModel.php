<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class abonentsModel extends Model
{
    use HasFactory;
    protected $table = 'abonents';
    
    
    public function insertAbonent($user_id, $tariff_id, $abonent_phone, $abonent_adress)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
         DB::table('abonents')->insert([
            'balance' => 0,
            'user_id' => $user_id,
            'tariff_id' => $tariff_id,
            'phone' => $abonent_phone,
            'adress' => $abonent_adress,
            'created_at' => $current_date_time
        ]);   
    }
    public static function getAbonentFirstUserId($id)
    {
       return abonentsModel::where('user_id', $id)->first();
        
    }
    public static function updateAbonent($abonent)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        DB::table('abonents')
                ->where('user_id', $abonent['user_id'])
                ->update([
                        'phone' => $abonent['phone'],
                        
                        'updated_at' => $current_date_time
                    ]);
    }
    public static function deleteTariff($user_id)
    {
        DB::table('abonents')->where('user_id', $user_id)->delete();
    }
}
