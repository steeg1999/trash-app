<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;




class tariffsModel extends Model
{
    protected $table = 'tariffs';
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

    use HasFactory;

    public static function GetTariffsInfo()
    {
        $tariffs = DB::table('tariffs')->select('name','price' ,'content')->get();
        return $tariffs;
    }
    public static function insert($tariff)
    {
        $current_date_time = \Carbon\Carbon::now()->toDateTimeString();
        $tariff['created_at'] = $current_date_time;
        DB::table('tariffs')->insert($tariff);
    }
    public static function GetTariffsName()
    {
        $tariffsName = DB::table('tariffs')->pluck('name');
        return $tariffsName->all();
    }
    public static function getIdByName($tariff_name)
    {
        $id = DB::table('tariffs')->where('name', $tariff_name)->value('id');
        return $id;
    }
    public static function getTariff($id)
    {
        return DB::table('tariffs')->where('id', $id)->first();
    }
    public static function getTariffs()
    {
        return DB::table('tariffs')->get();
    }
    public static function deleteTariff($id)
    {
        DB::table('tariffs')->where('id', $id)->delete();
    }
    public static function editTariff($tariff)
    {
        DB::table('tariffs')->where('id', $tariff['id'])->update($tariff);
    }

}
