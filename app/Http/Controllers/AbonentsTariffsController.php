<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Models\tariffsModel;
use App\Models\AbonentsTariffs;
use App\Models\Abonent;
use App\Models\Operation;
use App\Models\abonentsModel;


class AbonentsTariffsController extends Controller
{
    public static function addAdress(Request $request)
    {
        
        $validateFieldsAdd = $request->validate([
            'adress' => 'required'
        ]);
        $abonentTariff = new AbonentsTariffs();
        $adress= $request->only('adress');
        $adress=$adress['adress'];
        $tariff_id=$request->only('tariff_id');
        $tariff_id=$tariff_id['tariff_id'];
        if ($request->user_id!==null) {
            $user_id=$request->user_id;
            
        // $abonent_id = getAbonentFirstUserId
        $abonent_id = new abonentsModel();
        $abonent_id = $abonent_id->where('user_id', $user_id)->first();
        $abonent_id = $abonent_id->only('id');
        $abonent_id = $abonent_id['id'];
       
        $abonentTariff->adress =$adress;
        $abonentTariff->tariff_id = $tariff_id;
        $abonentTariff->abonent_id = $abonent_id;
        $abonentTariff->save();
        return redirect()->back();

        }
        else{
            $user_id=Auth::user()->only('id');
            $user_id=$user_id['id'];
        // $abonent_id = getAbonentFirstUserId
        $abonent_id = new abonentsModel();
        $abonent_id = $abonent_id->where('user_id', $user_id)->first();
        $abonent_id = $abonent_id->only('id');
        $abonent_id = $abonent_id['id'];
       
        $abonentTariff->adress =$adress;
        $abonentTariff->tariff_id = $tariff_id;
        $abonentTariff->abonent_id = $abonent_id;
        $abonentTariff->save();
        return redirect()->back();
        }


        return redirect('abonent');
        // dd($abonentTariff);
    }

    public static function delAdress($id)
    {
        AbonentsTariffs::where('id', $id)->delete();
        
        
           
        return redirect()->back();
    }
    public static function payAll()
    {
        $abonentsTariffs =AbonentsTariffs::all();
        foreach ($abonentsTariffs as $abonTariff) {

            $operation = new Operation();
            $operation->money = -1 * $abonTariff->tariff->price;
            $operation->abonent_id = $abonTariff->abonent->id;
            $operation->name ='Надання щомісячних послуг за адресю: ' . $abonTariff->adress;
            $operation->save();

            $abonTariff->abonent->balance = $abonTariff->abonent->balance - $abonTariff->tariff->price;
            $abonTariff->abonent->save();
        }
        return redirect()->back();
    }

}