<?php

namespace App\Http\Controllers;
use App\Models\abonentsModel;
use App\Models\tariffsModel;
use App\Models\Roles;
use App\Models\Abonent;
use App\Models\AbonentsCallback;
use App\Models\AbonentsTariffs;
use App\Models\Operation;
use App\Models\Transaction;
use App\Models\User;
use App\Http\Controllers\MainController;
use App\Http\Controllers\tariffsController;
use DB;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class adminController extends Controller
{
    
    public static function index()
    {
        
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        
       
        return view('AdminView', [
            'user'=>$user,
            'role'=>Roles::currentRole(),
            'tariffs' => tariffsModel::all(),
            'users'=>DB::table('users')->simplePaginate(10),
            'roles'=>Roles::all()
            ]);
    }

    public static function deleteUser($id, $role)
    {
        if($role == 'abonent'){
            $abonent = Abonent::firstWhere('user_id', $id);
            AbonentsTariffs::where('abonent_id', $abonent->id)->delete();
            Transaction::where('abonent_id', $abonent->id)->delete();
            Operation::where('abonent_id', $abonent->id)->delete();
            AbonentsCallback::where('abonent_id', $abonent->id)->delete();
            $abonent->delete();
            $user = User::find($id);
            $user->delete();
        }
        else{
            $user = User::find($id);
            $user->delete();

        }
        
        // usersController::deleteUser($id);
        return redirect('/admin');

    }
    public static function editUser(Request $request)
    {
        $validateFields = $request->validate([
            'user_name' => 'required',
            'user_email' => 'required|email',
        ]);
       
        $user = array(
            'id' => $request->input('user_id'),
            'name' => $request->input('user_name'),
            'email' => $request->input('user_email'),
            'password' => $request->input('user_password')
        );
        
        usersController::updateUser($user);
        return redirect('admin');

    
    

        // elseif ($role->role=="admin" || $role->role=="helper" ) {
        // }
        // $tariff = usersModel::getUser($id);
        
        // return view('EditUserView', ['user' => $user, 'role' => MainController::currentRole()]);
    }
}
