<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Models\Roles;
use App\Models\Transaction;
use App\Models\Operation;
use App\Models\Abonent;
use App\Models\User;
use App\Models\AbonentsCallback;
use App\Models\tariffsModel;
use Illuminate\Http\Request;
use Carbon\Carbon;

class helpController extends Controller
{
    
    public static function report()
    {
        $sumDebt=0;
        $sumAbonentDebt=0;
        $abonent = Abonent::all();
        $sumAbonent=0;
        $abonentDebt = $abonent->where('balance','<', '0');
        foreach ($abonentDebt as $abon) {
            $sumDebt-=$abon->balance;
            $sumAbonentDebt++;
        }
        foreach ($abonent as $abon) {
            $sumAbonent++;
        }
        
        $Carbon = new Carbon();
        $date = $Carbon->subDay()->format('m.d.Y');
        
        $operations=Operation::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('money');
        $operations=$operations*-1;
        $transactions=Transaction::where('created_at', '>=', Carbon::now()->startOfMonth())->sum('money');
        
        $result=array(
            'sumDebt'=>$sumDebt,
            'sumAbonentDebt'=>$sumAbonentDebt,
            'sumAbonent'=>$sumAbonent,
            'operations'=>$operations,
            'transactions'=>$transactions,
            'date'=>$date,
            
        );
        
        
            return view('Report', [
                'user'=>Auth::user(),
                'result'=>$result,
                'role'=>Roles::currentRole(),

                ]);
        
    }
    public static function sortData()
    {
        $sortCheck = route('sortDataDesc');
        $sortData = route('sortCheck');
      
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
 
        return view('HelpViewCallback', [
            'user'=>$user,
            'sortCheck'=>$sortCheck,
            'sortData'=>$sortData,
            'role'=>Roles::currentRole(),
            'callbacks'=>AbonentsCallback::orderBy('created_at')->simplePaginate(10),
            'roles'=>Roles::all()
            ]);
    }
    
    public static function sortDataDesc()
    {
        
      
        $sortCheck = route('sortData');
        $sortData = route('sortCheck');
        
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        
       
        return view('HelpViewCallback', [
            'user'=>$user,
            'sortCheck'=>$sortCheck,
            'sortData'=>$sortData,
            'role'=>Roles::currentRole(),
            'callbacks'=>AbonentsCallback::orderBy('created_at', 'DESC')->simplePaginate(10),
            'roles'=>Roles::all()
            ]);
    }

    public static function sortCheck()
    {
        
        $sortCheck = route('sortData');
        $sortData = route('sortCheckDesc');
        
    
        
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        
       
        return view('HelpViewCallback', [
            'user'=>$user,
            'sortCheck'=>$sortCheck,
            'sortData'=>$sortData,
            'role'=>Roles::currentRole(),
            'callbacks'=>AbonentsCallback::orderBy('status')->simplePaginate(10),
            'roles'=>Roles::all()
            ]);
    }

    public static function sortCheckDesc()
    {
        
        $sortCheck = route('sortData');
        $sortData = route('sortCheck');
    
        
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        
       
        return view('HelpViewCallback', [
            'user'=>$user,
            'sortCheck'=>$sortCheck,
            'sortData'=>$sortData,
            'role'=>Roles::currentRole(),
            'callbacks'=>AbonentsCallback::orderBy('status', 'DESC')->simplePaginate(10),
            'roles'=>Roles::all()
            ]);
    }

    public static function index()
    {
        
      
    
        
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        
       
        return view('HelpView', [
            'user'=>$user,
            'role'=>Roles::currentRole(),
            'tariffs'=>tariffsModel::all(),
            'users'=>User::where('role_id', 3)->orderBy('created_at', 'DESC')->simplePaginate(10),
            
            'roles'=>Roles::all()
            ]);
    }
    
    public static function confirmCallback(Request $request ,$id){
        $callback=AbonentsCallback::find($id);
        $callback->status=true;
        $callback->save();
        return redirect()->back();
    }
    public static function editUserView(Request $request ,$id, $role )
    {
        // dd($id, $role, $r);
        // dd($id);
            // $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
            // dd($user);
            $user = User::find($id)->only(['id', 'email', 'name', 'role_id']);
            // $user = User::firstWhere($id)->only(['id', 'email', 'name', 'role_id']);
        
        
        
        $abonent = new Abonent();
        $abonent = $abonent->where('user_id', $user['id'])->first();
        $abonentTariffs = AbonentsTariffs::where('abonent_id', $abonent->id)->get();
        

        $transaction = Transaction::where('abonent_id', $abonent->id);
        $operation = Operation::where('abonent_id', $abonent->id);
        // $paginate = array_multisort($paginate, $paginate->created_at);
        $paginate = $transaction->unionAll($operation)->orderBy('created_at', 'DESC')->simplePaginate(10);
        // $vivod = $goods1->unionAll($goods2)->paginate(12);

            return view('EditAbonentView', [
                'user'=>$user,
                'abonent'=>$abonent,
                'tariffs'=>tariffsModel::all(),
                'abonentTariffs'=>$abonentTariffs,
                'tariffsName'=>tariffsController::GetTariffsName(),
                'role'=>Roles::currentRole(),
                // 'operations'=>DB::table('operations')->where('abonent_id', $abonent['id'])->simplePaginate(10)
                'operations'=>$paginate
                ]);
        
    }
}
