<?php

namespace App\Http\Controllers;

use App\Models\abonentsModel;
use App\Models\tariffsModel;
use App\Models\Roles;
use App\Models\User;
use App\Models\Abonent;
use App\Models\AbonentsCallback;
use App\Models\AbonentsTariffs;
use App\Models\Operation;


use App\Models\Transaction;
use App\Http\Controllers\MainController;
use App\Http\Controllers\operationsController;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class abonentsController extends Controller
{
    public function insertAbonent($user_id, $tariff_id, $abonent_phone, $abonent_adress)
    {
        $abonent = new abonentsModel();
        $abonent->insertAbonent($user_id, $tariff_id, $abonent_phone, $abonent_adress);
    }
    public static function callback()
    {
        $user_id = Auth::user()->only('id');
        $abonent = Abonent::firstWhere('user_id', $user_id['id']);
        $abonentsCallback = new AbonentsCallback();
        $abonentsCallback->abonent_id = $abonent->id;
        $abonentsCallback->save();
        return redirect()->back();
    }
    public static function payBalanceIndex(){
        return view('PayBalanceView',['role'=>Roles::currentRole()]);
    }
    public static function payBalance(Request $request)
    {
        $validateFields = $request->validate([
            'balance' => 'required',
        ]);   
        $pay_balance = $request->input('balance');
        $user = Auth::user()->only('id');
        
        $abonent = new Abonent();
        $abonent = $abonent->where('user_id', $user['id'])->first();
        $abonent->increment('balance',$pay_balance);
        $transaction = new Transaction();
        $transaction->money = $pay_balance;
        $transaction->abonent_id = $abonent->id;
        $transaction->save();

        return redirect()->back();
    }
    
    public static function index()
    {
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        // dd($user);
        
        
        
        $abonent = new Abonent();
        $abonent = $abonent->where('user_id', $user['id'])->first();
        $abonentTariffs = AbonentsTariffs::where('abonent_id', $abonent->id)->get();
        

        $transaction = Transaction::where('abonent_id', $abonent->id);
        $operation = Operation::where('abonent_id', $abonent->id);
        // $paginate = array_multisort($paginate, $paginate->created_at);
        $paginate = $transaction->unionAll($operation)->orderBy('created_at', 'DESC')->simplePaginate(10);
        // $vivod = $goods1->unionAll($goods2)->paginate(12);

            return view('AbonentView', [
                'user'=>$user,
                'abonent'=>$abonent,
                'tariffs'=>tariffsModel::all(),
                'abonentTariffs'=>$abonentTariffs,
                'tariffsName'=>tariffsController::GetTariffsName(),
                'role'=>Roles::currentRole(),
                // 'operations'=>DB::table('operations')->where('abonent_id', $abonent['id'])->simplePaginate(10)
                'operations'=>$paginate
                ]);
        
    }
    public static function receipt()
    {
        $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
        
        
        $abonent = new Abonent();
        $abonent = $abonent->firstWhere('user_id', $user['id']);
        $abonentTariffs = $abonent->abonentTariffs->all();
        // dd($abonentTariffs[0]->tariff);
        $recomendPay =  $abonent->balance;
        $pay=0;
        foreach ($abonentTariffs as $trf) {
            // dd($trf->tariff->price);
            $recomendPay-=$trf->tariff->price;
            $pay+=$trf->tariff->price;
        }
        if($recomendPay>=0){
            $recomendPay=0;
        }
        else{
            $recomendPay = $recomendPay * -1 ;
        }
        // dd($recomendPay);
       

            return view('Receipt', [
                'user'=>$user,
                'abonent'=>$abonent,
                'tariffs'=>tariffsModel::all(),
                'abonentTariffs'=>$abonentTariffs,
                // 'tariffsName'=>tariffsController::GetTariffsName(),
                'role'=>Roles::currentRole(),
                'recomendPay'=>$recomendPay,
                'pay' => $pay
                
                ]);
        
    }
    
    public static function editAbonent(Request $request)
    {
        $validateFields = $request->validate([
            'user_name' => 'required',
            'user_email' => 'required|email',
            'abonent_phone' => 'required|numeric',
            
        ]);
        // dd($request);

        // "user_name" => "1"
        // "abonent_adress" => "1"
        // "abonent_phone" => "1"
        // "user_id" => "5"
        // "user_email" => "1@1"
        // "user_password" => null
        // "tariff_name" => "Домівка"

        // $user_id = $request->input('user_id');
        // $user_name = $request->input('user_name');
        // $user_email = $request->input('user_email');
		// $user_password = Hash::make($request->input('user_password'));
        // $abonent_phone = $request->input('abonent_phone');
        // $tariff_adress = $request->input('tariff_adress');
        // $tariff_name = $request->input('tariff_name');
        $abonent = array(
            'user_id'=> $request->input('user_id'),
            'phone' => $request->input('abonent_phone'),
           
            
        );
        $user = array(
            'id' => $request->input('user_id'),
            'name' => $request->input('user_name'),
            'email' => $request->input('user_email'),
            'password' => $request->input('user_password')
        );
        // dd($user);
        // dd($abonent, $request);
        abonentsModel::updateAbonent($abonent);
        usersController::updateUser($user);
        return redirect()->back();

    }
    
    public static function deleteAbonent($id)
    {
       
        if (User::find($id)->abonent->balance >= 0) {
            if (Auth::user()->role->role=="abonent") {  
                Auth::logout();
            }
            $abonent = Abonent::firstWhere('user_id', $id);
            AbonentsTariffs::where('abonent_id', $abonent->id)->delete();
            Transaction::where('abonent_id', $abonent->id)->delete();
            Operation::where('abonent_id', $abonent->id)->delete();
            AbonentsCallback::where('abonent_id', $abonent->id)->delete();
            $abonent->delete();
            $user = User::find($id);
            $user->delete();
            
        }
        if (Auth::user()->role->role=="helper") {
            return redirect('help');  
        }
        elseif (Auth::user()->role->role=="admin") {
            return redirect('admin');  
        }
        return redirect('/');
    }

    
    

}
