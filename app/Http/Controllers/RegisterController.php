<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\tariffsController;
use App\Http\Controllers\usersController;
use App\Http\Controllers\abonentsController;
use App\Models\User;
use App\Models\Roles;
use App\Models\AbonentsTariffs;
use App\Models\abonentsModel;
use Illuminate\Support\Facades\Route;
use App\Models\usersModel;

use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function CreateAbonent(Request $request)
    {
        if (Auth::user()) {
            
            if (Auth::user()->role->role=="abonent") {
                return redirect('abonent');  
            }
        }
        
        $validateFields = $request->validate([
            'user_name' => 'required',
            'user_email' => 'required|email',
            'user_password' => 'required',
            'abonent_phone' => 'required|numeric|max:12',
            'tariff_adress' => 'required'
        ]);
        if(usersController::uniqEmail($request->input('user_email'))){
            return redirect('/#registerForm')->withErrors([
                'email' => 'Ця електронна адраесса вже використовується'
            ]);
        }

        
        
       
        
        $user = new User;
        $user->name=$request->input('user_name');
        $user->email=$request->input('user_email');
        $user->password=Hash::make($request->input('user_password'));
        $user->role_id=Roles::getIdByRole('abonent');
        $user->save();
        
        // dd($user->id);
        
        $abonent = new abonentsModel();

        $abonent->phone = $request->input('abonent_phone');
        $abonent->balance = 0;
        $abonent->user_id = $user->id;

        $abonent->save();
       
        $abonentTariff = new AbonentsTariffs();
        
        $abonentTariff->adress = $request->input('tariff_adress');
        $abonentTariff->abonent_id =$abonent->id;
        $abonentTariff->tariff_id = $request->input('tariff_id');

        $abonentTariff->save();
        
        if (Auth::user()) {
            if (Auth::user()->role->role=="helper") {
                return redirect('help');  
            }
            
        }
        return redirect('/login');
        
    }
    public function CreateUser(Request $request)
    {
        $validateFields = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if(usersController::uniqEmail($request->input('user_email'))){
            return redirect('/#registerForm')->withErrors([
                'email' => 'Ця електронна адраесса вже використовується'
            ]);
        }
        $user = array(
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'role_id' => $request->input('role_id')
        );
        usersController::insert($user);
    
        return redirect(route('admin'));
        
    }
}
