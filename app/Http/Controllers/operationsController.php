<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\operationsModel;
class operationsController extends Controller
{
    public static function getAbonentOperations($abonent_id)
    {
        return operationsModel::getAbonentOperations($abonent_id);
    }
}
