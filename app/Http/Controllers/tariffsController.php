<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tariffsModel;
use App\Models\Roles;
use App\Http\Controllers\MainController;

class tariffsController extends Controller
{
    public static function createTariff(Request $request)
    {
        $validateFields = $request->validate([
            'name' => 'required',
            'price' => 'required',
            'content' => 'required',
            
        ]);
        

        $tariff =$request->only('name', 'price', 'content');
        // dd($tariff);    
        
        tariffsModel::insert($tariff);
        
        return redirect('/admin');
        }
    public static function index() {
        $tariffs = new tariffsModel();
        $tariffs = $tariffs->GetTariffsInfo();
        
        return view('tariffsView', ['tariffs' => $tariffs, 'role' => Roles::currentRole()]);
    }
    public static function GetTariffsName()
    {
        return tariffsModel::GetTariffsName();
    }
    public static function getIdByName($tariff_name)
    {
        return tariffsModel::getIdByName($tariff_name);
    } 
    public static function getTariff($id)
    {
        return tariffsModel::getTariff((int)$id);
    }
    public static function getTariffs()
    {
        return tariffsModel::getTariffs();
    }
    public static function deleteTariff($id)
    {
        tariffsModel::deleteTariff($id);
        return redirect('/admin');

    }
    public static function editTariff(Request $tariff)
    {
        $tariff = $tariff->only('id','name', 'price', 'content');
        
        tariffsModel::editTariff($tariff);
        return redirect('/admin');

    }
    public static function editTariffView($id)
    {
        $tariff = tariffsModel::getTariff($id);
        
        return view('EditTariffView', ['tariff' => $tariff, 'role' => Roles::currentRole()]);
    }
}
