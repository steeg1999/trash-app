<?php

namespace App\Http\Controllers;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MainController;
use Illuminate\Http\Request;


class LoginController extends Controller
{
   /**
     * Обработка попыток аутентификации.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {   
        
        
        if(Auth::check()){
            if(Roles::getRoleById($role_id)=='abonent'){
                return redirect('abonent');
            }
            elseif(Roles::getRoleById($role_id)=='admin'){
                // dd('admin login');
            return redirect('admin');
            }
            elseif(Roles::getRoleById($role_id)=='helper'){
            return redirect('help');
            }
        }
        
        $credentials = $request->only(['email', 'password']);
        

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            $role_id = Auth::user()->only('role_id');
            
            if(Roles::getRoleById($role_id)=='abonent'){
                return redirect('abonent');
            }
            elseif(Roles::getRoleById($role_id)=='admin'){
                // dd('admin login');
            return redirect('admin');
            }
            elseif(Roles::getRoleById($role_id)=='helper'){
            return redirect('help');
            }
            
        }

        return back()->withErrors([
            'email' => 'Введені неправельні данні',
        ]);
    }
     
    public function index() {
        // dd(Roles::getRoleById(1),Roles::getIdByRole('admin'));
        
        if(Auth::check()){
            return redirect('/');
        }
        return view('LoginView', ['role'=>Roles::currentRole()]);
    }
    public function logout(Type $var = null)
    {
        Auth::logout();
        return redirect('/');
    }



}
