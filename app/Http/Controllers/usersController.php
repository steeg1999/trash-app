<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\usersModel;
use App\Models\Abonent;
use App\Models\User;
use App\Models\AbonentsTariffs;
use App\Models\Transaction;
use App\Models\Operation;
use App\Models\tariffsModel;
use App\Models\Roles;

class usersController extends Controller
{
    use HasFactory, Notifiable;
    public function insertUserAbonent($name, $email, $password)
    {
        $id = new usersModel();
        return $id->addUserAbonent($name, $email, $password);
    }
    public static function insert($user)
    {
        usersModel::insert($user);
    }
    public static function uniqEmail($email)
    {
        if(usersModel::uniqEmail($email)){
        return true;
        }
        else{
        return false;
        }
    
    }

    
    public static function updateUser($user)
    {
        if($user['password']==null){

            usersModel::updateUserNoPassword($user);
        }
        else{
            usersModel::updateUser($user);
        }
    }
    
    public static function searchAbonent(Request $request)
    {
        $validateFields = $request->validate([
            'abonent_id' => 'required|numeric',           
        ]);
        $abon = Abonent::find($request->abonent_id);
        
        if ($abon!==null) {
            return redirect(route('editUserView', [$abon->user_id, 'abonent']));
        }
        else{
            return redirect('help');
        }
    }
    public static function editUserView(Request $request ,$id, $role )
    {
        
        // dd($id, $role, $r);
        // dd($id);
        if ($role == 'abonent') {
            // $user = Auth::user()->only(['id', 'email', 'name', 'role_id']);
            // dd($user);
            $user = User::find($id)->only(['id', 'email', 'name', 'role_id']);
            // $user = User::firstWhere($id)->only(['id', 'email', 'name', 'role_id']);
        
        
        
        $abonent = new Abonent();
        $abonent = $abonent->where('user_id', $user['id'])->first();
        $abonentTariffs = AbonentsTariffs::where('abonent_id', $abonent->id)->get();
        

        $transaction = Transaction::where('abonent_id', $abonent->id);
        $operation = Operation::where('abonent_id', $abonent->id);
        // $paginate = array_multisort($paginate, $paginate->created_at);
        $paginate = $transaction->unionAll($operation)->orderBy('created_at', 'DESC')->simplePaginate(10);
        // $vivod = $goods1->unionAll($goods2)->paginate(12);

            return view('EditAbonentView', [
                'user'=>$user,
                'abonent'=>$abonent,
                'tariffs'=>tariffsModel::all(),
                'abonentTariffs'=>$abonentTariffs,
                'tariffsName'=>tariffsController::GetTariffsName(),
                'role'=>Roles::currentRole(),
                // 'operations'=>DB::table('operations')->where('abonent_id', $abonent['id'])->simplePaginate(10)
                'operations'=>$paginate
                ]);
        }
        elseif ($role =='admin'||'helper') {
            
            return view('EditUserView', [
                'user'=>User::find($id),
                'role'=>Roles::currentRole(),
                
                ]);
        
        }    
    
    
    


    }
}