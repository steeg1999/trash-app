<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\tariffsController;
use Auth;
use DB;
use App\Models\Roles;
use App\Models\tariffsModel;


class MainController extends Controller
{
    
    public function index() {
    //   dd(tariffsModel::all('id','name'));
            
        return view('MainView', [
            'tariffs' => tariffsModel::all('id','name'),
             'role' => Roles::currentRole()]);
    }

    public function make() {
        
        if (empty(DB::table('roles')->get()->toArray())){
        

            DB::table('roles')->insert([
                'role' => 'admin',
            ]);
            DB::table('roles')->insert([
                'role' => 'helper',
            ]);
            DB::table('roles')->insert([
                'role' => 'abonent',
            ]);
            DB::table('users')->insert([
                'name' => 'Адміністратор',
                'email' => 'admin@admin',
                'role_id' => 1,
                'password' => Hash::make('password'),
            ]);
        
            DB::table('users')->insert([
                'name' => 'Підтримка кліента',
                'email' => 'help@help',
                'password' => Hash::make('password'),
                'role_id' => 2,
            ]);
            return "Стартові нааштування виконані";
        }
        else{
            return "Налашттування не потребуються";
        }
    }
    public function indexContacts() {
        $tarrifsName = tariffsController::GetTariffsName();
                
        return view('ContactsView', ['role' => Roles::currentRole()]);
    }
    
    

    
}
