<?php

namespace App\Http\Middleware;

use App\Models\Roles;
use Closure;
use Illuminate\Http\Request;
use Auth;

class abonent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role = Roles::getRoleById(Auth::user()->only(['role_id']));
        
        if($role=='abonent'){
        return $next($request);
        }
        return redirect('/');
    }
}
