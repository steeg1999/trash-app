<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Roles;
Use Auth;

class adminOrHelper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role = Roles::getRoleById(Auth::user()->only(['role_id']));
        
        if($role=='admin'||'helper'){
        return $next($request);
        }
        return redirect('/');
    }
}
