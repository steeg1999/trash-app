<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Roles;
Use Auth;

class abonentOrAdminOrHelper
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role = Roles::getRoleById(Auth::user()->only(['role_id']));
        
        if($role=='abonent'||'admin'||'helper'){
        return $next($request);
        }
        return redirect('/');
    }
}
