let MainCarousel = document.querySelector('#MainCarousel')
let maincarousel = new bootstrap.Carousel(MainCarousel, {
    interval: 2000,
    wrap: false
});

let main_form_checkbox_status = false;
let main_form_checkbox_border = document.querySelector("#checkbox-border");
let main_form_checkbox = document.querySelector("#checkbox");
let main_form_button = document.querySelector("#main-form-button");
let main_form_submit = document.querySelector("#main-form-submit");
main_form_checkbox.style.display = "none";
console.dir(document.querySelector("#checkbox"));
console.dir(document.querySelector("#checkbox-border"));

main_form_checkbox_border.addEventListener('click', event => {
    if (main_form_checkbox_status == false) {
        main_form_checkbox_status = true;
        main_form_checkbox.style.display = "block";
    } else if (main_form_checkbox_status == true) {
        main_form_checkbox.style.display = "none";
        main_form_checkbox_status = false;

    }
});
main_form_button.addEventListener('click', event => {
    if (main_form_checkbox_status == true)
        main_form_submit.click();

});