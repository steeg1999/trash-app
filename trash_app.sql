-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 15 2021 г., 03:54
-- Версия сервера: 8.0.19
-- Версия PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `trash_app`
--

-- --------------------------------------------------------

--
-- Структура таблицы `abonents`
--

CREATE TABLE `abonents` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` bigint NOT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `user_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `abonents`
--

INSERT INTO `abonents` (`id`, `created_at`, `updated_at`, `phone`, `balance`, `user_id`) VALUES
(10, '2021-05-24 08:33:55', '2021-06-14 21:13:26', 968535629, -200.00, 13),
(13, '2021-05-24 08:43:28', '2021-06-14 21:13:26', 968563548, -300.00, 16),
(14, '2021-05-24 08:57:01', '2021-06-14 21:13:26', 965878645, 0.00, 17),
(15, '2021-05-24 08:57:40', '2021-06-14 21:49:46', 99659, 300.00, 18),
(16, '2021-05-24 09:00:04', '2021-06-14 21:50:03', 966549821, 100.00, 19),
(17, '2021-05-26 13:34:10', '2021-06-14 21:13:26', 96516465, 350.00, 22),
(18, '2021-05-26 14:58:02', '2021-05-26 14:58:02', 9654641, 0.00, 24),
(19, '2021-05-26 14:59:00', '2021-05-26 14:59:00', 915165, 0.00, 25),
(20, '2021-05-26 15:00:26', '2021-05-26 15:00:26', 95616, 0.00, 26),
(21, '2021-05-26 15:19:23', '2021-05-26 15:19:23', 2131232, 0.00, 27),
(22, '2021-05-26 15:20:16', '2021-06-14 21:18:00', 96854726, -100.00, 28);

-- --------------------------------------------------------

--
-- Структура таблицы `abonents_callback`
--

CREATE TABLE `abonents_callback` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `abonent_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `abonents_callback`
--

INSERT INTO `abonents_callback` (`id`, `created_at`, `updated_at`, `status`, `abonent_id`) VALUES
(4, '2021-05-15 12:40:26', '2021-05-24 08:57:11', 0, 14),
(5, '2021-05-18 06:43:21', '2021-05-24 08:57:13', 0, 14),
(6, '2021-05-24 11:46:19', '2021-05-24 09:38:50', 1, 14),
(7, '2021-05-24 06:39:19', '2021-05-24 08:57:14', 0, 14),
(8, '2021-05-18 06:54:36', '2021-05-24 08:57:15', 0, 14),
(9, '2021-05-24 09:25:25', '2021-05-24 09:38:53', 1, 14),
(10, '2021-05-24 09:00:18', '2021-05-24 09:38:26', 1, 16),
(11, '2021-05-24 12:17:33', '2021-05-24 09:00:20', 0, 16),
(12, '2021-05-24 08:10:28', '2021-05-24 09:38:55', 1, 16),
(13, '2021-05-24 09:40:08', '2021-05-24 10:04:29', 1, 14),
(14, '2021-05-24 09:40:09', '2021-05-29 12:49:20', 1, 14),
(15, '2021-05-24 09:40:10', '2021-05-29 12:49:19', 1, 14),
(16, '2021-05-24 09:40:11', '2021-05-24 09:40:11', 0, 14),
(17, '2021-05-24 09:40:11', '2021-05-29 12:49:17', 1, 14),
(18, '2021-05-24 09:40:12', '2021-05-24 09:40:12', 0, 14),
(19, '2021-05-24 09:40:13', '2021-05-24 09:40:13', 0, 14),
(20, '2021-05-24 09:40:13', '2021-05-24 09:43:39', 1, 14),
(21, '2021-05-24 09:40:13', '2021-06-14 21:48:36', 1, 14),
(22, '2021-05-24 09:40:14', '2021-05-24 09:40:14', 0, 14),
(23, '2021-05-24 09:40:14', '2021-05-24 09:40:14', 0, 14),
(25, '2021-06-14 21:50:20', '2021-06-14 21:50:20', 0, 14);

-- --------------------------------------------------------

--
-- Структура таблицы `abonents_tariffs`
--

CREATE TABLE `abonents_tariffs` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `adress` varchar(255) NOT NULL,
  `tariff_id` bigint UNSIGNED NOT NULL,
  `abonent_id` bigint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `abonents_tariffs`
--

INSERT INTO `abonents_tariffs` (`id`, `created_at`, `updated_at`, `adress`, `tariff_id`, `abonent_id`) VALUES
(18, '2021-05-24 08:33:55', '2021-05-24 08:33:55', '123123', 1, 10),
(21, '2021-05-24 08:43:28', '2021-05-24 08:43:28', '1212321', 1, 13),
(22, '2021-05-24 08:57:01', '2021-05-24 08:57:01', 'Будывля 6456', 1, 14),
(23, '2021-05-24 08:57:40', '2021-05-24 08:57:40', '12123', 1, 15),
(24, '2021-05-24 09:00:04', '2021-05-24 09:00:04', '32123', 1, 16),
(25, '2021-05-26 12:52:52', '2021-05-26 12:52:52', 'Вул. Грушевського буд. 7', 3, 14),
(26, '2021-05-26 13:34:10', '2021-05-26 13:34:10', 'Вул. Пушкіна Буд. 231', 1, 17),
(27, '2021-05-26 13:34:29', '2021-05-26 13:34:29', 'Вул. Пушкіна Буд. 232', 3, 17),
(28, '2021-05-26 14:59:36', '2021-05-26 14:59:36', 'sadsd', 1, 13),
(29, '2021-05-26 15:20:16', '2021-05-26 15:20:16', '223213', 1, 22),
(31, '2021-05-30 22:16:29', '2021-05-30 22:16:29', 'Вул. Грушевського буд. 10 кв.77', 2, 14);

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(51, '2013_05_12_121855_create_roles_table', 1),
(52, '2014_10_12_000000_create_users_table', 1),
(53, '2014_10_12_100000_create_password_resets_table', 1),
(54, '2019_08_19_000000_create_failed_jobs_table', 1),
(55, '2021_05_15_174731_create_tariffs_table', 1),
(56, '2021_05_15_182635_create_abonents_table', 1),
(57, '2021_05_15_182636_create_abonents_tariffs_table', 1),
(58, '2021_05_15_182811_create_operations_table', 1),
(59, '2021_05_21_122154_create_abonents_callback_table', 1),
(60, '2021_05_21_122344_create_transactions_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `operations`
--

CREATE TABLE `operations` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `money` double(8,2) NOT NULL,
  `abonent_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Щомісячний платіж за тарифом'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `operations`
--

INSERT INTO `operations` (`id`, `created_at`, `updated_at`, `money`, `abonent_id`, `name`) VALUES
(8, '2021-05-24 12:04:55', '2021-05-24 12:04:55', -100.00, 10, 'Надання щомісячних послуг за адресю: 123123'),
(9, '2021-05-24 12:04:55', '2021-05-24 12:04:55', -100.00, 13, 'Надання щомісячних послуг за адресю: 1212321'),
(10, '2021-05-24 12:04:55', '2021-05-24 12:04:55', -100.00, 14, 'Надання щомісячних послуг за адресю: Будывля 6456'),
(11, '2021-05-24 12:04:55', '2021-05-24 12:04:55', -100.00, 15, 'Надання щомісячних послуг за адресю: 12123'),
(12, '2021-05-24 12:04:55', '2021-05-24 12:04:55', -100.00, 16, 'Надання щомісячних послуг за адресю: 32123'),
(13, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 10, 'Надання щомісячних послуг за адресю: 123123'),
(14, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 13, 'Надання щомісячних послуг за адресю: 1212321'),
(15, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 14, 'Надання щомісячних послуг за адресю: Будывля 6456'),
(16, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 15, 'Надання щомісячних послуг за адресю: 12123'),
(17, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 16, 'Надання щомісячних послуг за адресю: 32123'),
(18, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -50.00, 14, 'Надання щомісячних послуг за адресю: Вул. Грушевського буд. 7'),
(19, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 17, 'Надання щомісячних послуг за адресю: Вул. Пушкіна Буд. 231'),
(20, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -50.00, 17, 'Надання щомісячних послуг за адресю: Вул. Пушкіна Буд. 232'),
(21, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 13, 'Надання щомісячних послуг за адресю: sadsd'),
(22, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -100.00, 22, 'Надання щомісячних послуг за адресю: 223213'),
(23, '2021-06-14 21:13:26', '2021-06-14 21:13:26', -50.00, 14, 'Надання щомісячних послуг за адресю: Вул. Грушевського буд. 10 кв.77');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` bigint UNSIGNED NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'helper', NULL, NULL),
(3, 'abonent', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tariffs`
--

CREATE TABLE `tariffs` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price` double(8,2) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tariffs`
--

INSERT INTO `tariffs` (`id`, `created_at`, `updated_at`, `name`, `price`, `content`) VALUES
(1, '2021-05-23 11:17:56', NULL, 'Домівка', 100.00, 'Ліс ще дрімає в передранішній тиші... Непорушне стоять дерева, загорнені в сутінь, рясно вкриті краплистою росою. Тихо навкруги, мертво... Лиш де-не-де прокинеться пташка, непевним голосом обізветься зі свого затишку. Ліс ще дрімає... а з синім небом вже щось діється: воно то зблідне, наче від жаху, то спахне сяйвом, немов од радощів. Небо міниться, небо грає усякими барвами, блідим сяйвом торкає вершечки чорного лісу... Стрепенувся врешті ліс і собі заграв... Зашепо'),
(2, '2021-05-23 11:18:06', NULL, 'Квартира', 50.00, 'Ні, не бреши, старий, не всі... На твоїй довгій тисячолітній ниві життєвій не одна стрівалась істота, що сміливо зводила очі догори, відважно зазирала тобі в вічі, й тоді... о, тоді гарно було обом нам... Бо смільчак, перевірившись, що жахався по-дурному, набиравсь нової, ще більшої відваги, а ти, старий, чув, що, може, незабаром даси спокій натрудженим кісткам... Жахаються, а не знають, що страх тільки й істніє на світі полохливістю других, що старий Хо порохом розсипав би ся, коли б усе живуще хоч раз зважилося глянути йому в вічі... Хе-хе! Дурні, дурні!.. Тільки старі ноги труджу через дурнів... От як втомився!.. е-е!..'),
(3, '2021-05-23 11:18:18', NULL, 'Офіс', 50.00, 'Хо сидить посеред галяви, а навкруги його панує мертва, прикра тиша. Все живе, затаївши духа, не співає, не кричить, не ворушиться, не жиє. Від ведмедя до мурашки — все спаралізовано страхом. Рослини бояться навіть тягти сік із землі, пити холодну росу, виправити зібгані листочки, розгорнути звинені на ніч квітки. Пустотливий парус сонця зупиняється в зеленій гущині, і лиш здалеку придивляється до сивої, мов туман той, бороди діда Хо, і не відважується наблизитись, невважаючи на непереможне бажання попустувати з тою бородою...'),
(7, '2021-05-30 22:21:36', NULL, 'Еко 3 в 1', 50.00, 'А ліс ще якусь хвилинку стояв нерухомий, мов мертвий. Далі — дерева затремтіли, стрепенулись, розгорнули листочки... Промінь стрибнув на полянку просто до звинених квіток, пташки заспівали, комашня заметушилася, ліс загомонів, природа знов віджила.');

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `money` double(8,2) NOT NULL,
  `abonent_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT 'Поповнення рахунку'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transactions`
--

INSERT INTO `transactions` (`id`, `created_at`, `updated_at`, `money`, `abonent_id`, `name`) VALUES
(13, '2021-05-24 12:20:56', '2021-05-24 12:20:56', 100.00, 14, 'Поповнення рахунку'),
(14, '2021-05-24 12:21:01', '2021-05-24 12:21:01', 200.00, 14, 'Поповнення рахунку'),
(15, '2021-05-26 13:34:36', '2021-05-26 13:34:36', 500.00, 17, 'Поповнення рахунку'),
(18, '2021-06-14 21:49:46', '2021-06-14 21:49:46', 500.00, 15, 'Поповнення рахунку'),
(19, '2021-06-14 21:50:03', '2021-06-14 21:50:03', 300.00, 16, 'Поповнення рахунку');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint UNSIGNED NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `role_id`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Іванов Іван Івановиич', 'admin@admin', NULL, 1, '$2y$10$kcQA03/8uC/wGlnlnrmeTOp4bDVM33aaDKifxSrlwsHtrcVpWnA4u', NULL, NULL, NULL),
(2, 'Петр Івановиич Зю', 'help@help', NULL, 2, '$2y$10$WHJHy76KQm66drHpIkJu..bKEd03rt.KNsx47H1evbcdOusM5N7mS', NULL, NULL, '2021-05-23 17:59:50'),
(13, 'Івнов Іван Дмитрович', 'qwerty@gmail.com', NULL, 3, '$2y$10$zYHoUhdh57/420uBih4YGu80x8uynIv8L56v2hwpelmQjXH9oHCEK', NULL, '2021-05-24 08:33:55', '2021-06-14 21:09:32'),
(16, 'Бутирин Дмитро Антонович', 'asdfg@gmail.com', NULL, 3, '$2y$10$mTpnI83qhzX6anUTpnmBBeA6bcRtHD62.Q4Op0PDrAlI66ii4/T9.', NULL, '2021-05-24 08:43:28', '2021-06-14 21:13:15'),
(17, 'Петр Петрович Петров', '1@1', NULL, 3, '$2y$10$U5ACP6o4M1l4ReADNpwIQe54Rg0d4vWNYuh/1Yx4TDa7V.hCL7dHu', NULL, '2021-05-24 08:57:01', '2021-05-24 08:57:01'),
(18, 'Шимон Павло Дмитрович', '2@2', NULL, 3, '$2y$10$SUHuI6qBJ3H9oBtlWfUUIua5vak4s64bR1uhQ0B6SO/XkfgkJFmy2', NULL, '2021-05-24 08:57:40', '2021-06-14 21:11:55'),
(19, 'Олексенко Вiталiй Антонович', '3@3', NULL, 3, '$2y$10$EF4ugxLSdZ6R5wV7.pHXNuj/iaNlq3IRFfzMA.gFkzi6fP.u.YiXS', NULL, '2021-05-24 09:00:04', '2021-06-14 21:10:53'),
(20, 'Петр петрович петров', 'asda@wseqdw.com', NULL, 2, '$2y$10$W4aBOJRrivPZiAea52.xA.1QujxaqbzL//NT/Yl4FeCA3r93ZUmrm', NULL, '2021-05-26 13:32:33', NULL),
(21, 'Дмитро дмитрович Зи', 'sdasd@wqe', NULL, 2, '$2y$10$lqTIVbyrBKMH1YBqpoe6K.1ZjoNxeRImrzeEh7vgJ3L799k.vbRWu', NULL, '2021-05-26 13:33:05', NULL),
(22, 'Юлія ОЛегівна Дівіф', '123@1233', NULL, 3, '$2y$10$VSRajAUcjMWphuh1IpW.u.JyllJQMKJ6MCcXZiihjo.VpV.hNKaUS', NULL, '2021-05-26 13:34:10', '2021-05-26 13:34:10'),
(23, 'KDJFSSKdkfsdfkd', 's@w', NULL, 1, '$2y$10$L0yL9x7J.vPvXW.C97nO3uUMgMhNSh/ibv.Kxir.LNoeJdwydyFeW', NULL, '2021-05-26 13:35:35', NULL),
(24, 'Lfsdlfksdl;f', '12322@2122', NULL, 3, '$2y$10$ANLDlkVFVclzmYpjTAxoZ.m7IBwpy6pl/VP2woJDOPmWXmbNqFH9y', NULL, '2021-05-26 14:58:02', '2021-05-26 14:58:02'),
(25, 'ASDdss', '4@4', NULL, 3, '$2y$10$3MjFO/am9prbTMwlPTQBqemZOzxpahrvVhCk3N32qi7cdJlSceEam', NULL, '2021-05-26 14:59:00', '2021-05-26 14:59:00'),
(26, 'sddsds', '342@w', NULL, 3, '$2y$10$SI3yBiwLVEW5HK6E.wlep.fjU9/nt2.OcnsqrT1tjOn6BmceX798.', NULL, '2021-05-26 15:00:26', '2021-05-26 15:00:26'),
(27, '123123', '2@3312', NULL, 3, '$2y$10$jrs0orLdAap65Q/gECKPGeOkL0lmJFj1V1fCXq7ACvJLUkNu3duNu', NULL, '2021-05-26 15:19:23', '2021-05-26 15:19:23'),
(28, 'Дзюба Анна Павленко', 'sdfm22mw@33222.com', NULL, 3, '$2y$10$lGnzc1u4R/gCanqQeNZNPOzYXBT1s4qNBhWmdvSq0UKfjgXfngYcC', NULL, '2021-05-26 15:20:16', '2021-06-14 21:18:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `abonents`
--
ALTER TABLE `abonents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abonents_user_id_foreign` (`user_id`);

--
-- Индексы таблицы `abonents_callback`
--
ALTER TABLE `abonents_callback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abonents_callback_abonent_id_foreign` (`abonent_id`);

--
-- Индексы таблицы `abonents_tariffs`
--
ALTER TABLE `abonents_tariffs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abonents_tariffs_tariff_id_foreign` (`tariff_id`),
  ADD KEY `abonents_tariffs_abonent_id_foreign` (`abonent_id`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `operations`
--
ALTER TABLE `operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operations_abonent_id_foreign` (`abonent_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tariffs`
--
ALTER TABLE `tariffs`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_abonent_id_foreign` (`abonent_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `abonents`
--
ALTER TABLE `abonents`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `abonents_callback`
--
ALTER TABLE `abonents_callback`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT для таблицы `abonents_tariffs`
--
ALTER TABLE `abonents_tariffs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT для таблицы `operations`
--
ALTER TABLE `operations`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `tariffs`
--
ALTER TABLE `tariffs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `abonents`
--
ALTER TABLE `abonents`
  ADD CONSTRAINT `abonents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `abonents_callback`
--
ALTER TABLE `abonents_callback`
  ADD CONSTRAINT `abonents_callback_abonent_id_foreign` FOREIGN KEY (`abonent_id`) REFERENCES `abonents` (`id`);

--
-- Ограничения внешнего ключа таблицы `abonents_tariffs`
--
ALTER TABLE `abonents_tariffs`
  ADD CONSTRAINT `abonents_tariffs_abonent_id_foreign` FOREIGN KEY (`abonent_id`) REFERENCES `abonents` (`id`),
  ADD CONSTRAINT `abonents_tariffs_tariff_id_foreign` FOREIGN KEY (`tariff_id`) REFERENCES `tariffs` (`id`);

--
-- Ограничения внешнего ключа таблицы `operations`
--
ALTER TABLE `operations`
  ADD CONSTRAINT `operations_abonent_id_foreign` FOREIGN KEY (`abonent_id`) REFERENCES `abonents` (`id`);

--
-- Ограничения внешнего ключа таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_abonent_id_foreign` FOREIGN KEY (`abonent_id`) REFERENCES `abonents` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
