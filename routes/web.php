<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\usersController;
use App\Http\Controllers\tariffsController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\abonentsController;
use App\Http\Controllers\adminController;
use App\Http\Controllers\helpController;
use App\Http\Controllers\AbonentsTariffsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('home');
Route::get('/contacts', [MainController::class, 'indexContacts']);
Route::get('/tariffs', [tariffsController::class, 'index']);
Route::get('tariffsController/GetTariffsName', [tariffsController::class, 'GetTariffsName']);
Route::post('RegisterController/CreateAbonent', [RegisterController::class, 'CreateAbonent'])->name('CreateAbonent');
Route::post('RegisterController/CreateUser', [RegisterController::class, 'CreateUser'])->name('CreateUser');
Route::post('tariffsController/createTariff', [tariffsController::class, 'createTariff'])->middleware('admin')->name('createTariff');
Route::post('abonentController/editAbonent', [abonentsController::class, 'editAbonent'])->middleware('abonentOrAdminOrHelper')->name('editAbonent');
Route::post('AbonentsTariffsController/addAdress', [AbonentsTariffsController::class, 'addAdress'])->middleware('abonentOrAdminOrHelper')->name('addAdress');
Route::get('AbonentsTariffsController/payAll', [AbonentsTariffsController::class, 'payAll'])->middleware('admin')->name('payAll');
Route::get('AbonentsTariffsController/delAdress/{id}', [AbonentsTariffsController::class, 'delAdress'])->middleware('abonentOrAdminOrHelper')->name('delAdress');
Route::get('abonentController/payBalanceIndex', [abonentsController::class, 'payBalanceIndex'])->middleware('abonentOrAdminOrHelper')->name('payBalanceIndex');
Route::post('abonentController/payBalance', [abonentsController::class, 'payBalance'])->middleware('abonentOrAdminOrHelper')->name('payBalance');
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::get('/make', [MainController::class, 'make'])->name('make');
Route::post('LoginController/login', [LoginController::class, 'login']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/abonent', [abonentsController::class, 'index'])->middleware('auth')->middleware('abonent')->name('abonent');
Route::get('/admin', [adminController::class, 'index'])->middleware('auth')->middleware('admin')->name('admin');
Route::get('/tariffsController/delete/{id}', [tariffsController::class, 'deleteTariff'])->middleware('auth')->middleware('admin')->name('deletTariff');
Route::get('/adminController/delete/{id}/{role}', [adminController::class, 'deleteUser'])->middleware('auth')->middleware('admin')->name('deleteUser');
Route::get('/abonentsController/delete/{id}', [abonentsController::class, 'deleteAbonent'])->middleware('auth')->middleware('abonentOrAdminOrHelper')->name('deleteAbonent');
Route::get('/abonentsController/callback', [abonentsController::class, 'callback'])->middleware('auth')->middleware('abonentOrAdminOrHelper')->name('callback');
Route::get('/abonentsController/receipt', [abonentsController::class, 'receipt'])->middleware('auth')->middleware('abonentOrAdminOrHelper')->name('receipt');
Route::get('/helpController/report', [helpController::class, 'report'])->middleware('auth')->middleware('helper')->name('report');
Route::get('/tariffsController/edit/{id}', [tariffsController::class, 'editTariffView'])->middleware('auth')->middleware('admin')->name('editTariffView');
Route::get('/helpController/confirmCallback/{id}', [helpController::class, 'confirmCallback'])->middleware('auth')->middleware('helper')->name('confirmCallback');
Route::get('/usersController/editUserView/{id}/{role}', [usersController::class, 'editUserView'])->middleware('auth')->middleware('adminOrHelper')->name('editUserView');
Route::post('/usersController/searchAbonent', [usersController::class, 'searchAbonent'])->middleware('auth')->middleware('adminOrHelper')->name('searchAbonent');
// Route::get('/helpController/editUserView/{id}/{role}', [helpController::class, 'editUserView'])->middleware('auth')->middleware('help')->name('helpEditUserView');
Route::post('adminController/editUser', [adminController::class, 'editUser'])->middleware('admin')->name('editUser');
Route::post('/tariffsController/editsubmit', [tariffsController::class, 'editTariff'])->middleware('auth')->middleware('admin')->name('editTariffSubmit');
Route::get('/help', [helpController::class, 'index'])->middleware('auth')->middleware('helper')->name('help');
Route::get('/help/callback/data', [helpController::class, 'sortData'])->middleware('auth')->middleware('helper')->name('sortData');
Route::get('/help/callback/data/desc', [helpController::class, 'sortDataDesc'])->middleware('auth')->middleware('helper')->name('sortDataDesc');
Route::get('/help/callback/check', [helpController::class, 'sortCheck'])->middleware('auth')->middleware('helper')->name('sortCheck');
Route::get('/help/callback/check/desc', [helpController::class, 'sortCheckDesc'])->middleware('auth')->middleware('helper')->name('sortCheckDesc');
Route::get('/logout', function() {
    Auth::logout();
    return redirect('/');
});


