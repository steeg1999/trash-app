<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbonentsTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abonents_tariffs', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('adress');
            $table->foreignId('tariff_id')->constrained('tariffs');
            $table->foreignId('abonent_id')->constrained('abonents');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abonents_tariffs');
    }
}
