<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role' => 'admin',
        ]);
        DB::table('roles')->insert([
            'role' => 'helper',
        ]);
        DB::table('roles')->insert([
            'role' => 'abonent',
        ]);
        DB::table('users')->insert([
            'name' => 'Іванов Іван Івановиич',
            'email' => 'admin@admin',
            'role_id' => 1,
            'password' => Hash::make('password'),
        ]);
    
        DB::table('users')->insert([
            'name' => 'Петр Івановиич Зю',
            'email' => 'help@help',
            'password' => Hash::make('password'),
            'role_id' => 2,
        ]);
        
    }
}
